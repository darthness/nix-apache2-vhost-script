#!/bin/bash
txtred=$(tput setaf 1)
txtrst=$(tput sgr0)
txtbld=$(tput bold)
bold=${txtbld}

# the primary dir is the root of your documentRoot
PRIMARYDIR=/var/www
VHOST_DIR=/etc/vhosts

#settings
APACHE_PORT=80

if [ ! -w /etc/hosts ] ; then
   echo -e "${txtred} Please run this script as sudo! ${txtrst}"
   exit;
fi



echo -n "Please enter the project name (for the directory): "
read PROJECTNAME
echo ---  ${bold}PROJECTNAME SET TO $PROJECTNAME${txtrst}
echo 
echo -n "Please enter the domain (for the URL) ${txtred}${txtbld} NO WWW:${txtrst} "
read PROJECTDOMAIN
echo
echo ---  DOMAIN SET TO $PROJECTDOMAIN
echo ===================================================


if [ ! -r $PRIMARYDIR/$PROJECTNAME ] ; then
    mkdir $PRIMARYDIR/$PROJECTNAME    
fi

echo ===================================================
echo -n "Use public dir? (y/n) "
read USEPUB

if [ $USEPUB == '' ] || [ $USEPUB == 'n' ] ; then 
   PUBDIR=/
else 
   PUBDIR=/public/
    if [ ! -r $PRIMARYDIR/$PROJECTNAME/public ] ; then
        mkdir $PRIMARYDIR/$PROJECTNAME/public
    fi
fi
echo ===================================================
echo --- Creating vhost
echo ===================================================

if [ -r $VHOST_DIR/vhost_$PROJECTNAME.conf ] ; then
        echo "The selected project vhost already exists!";
        exit
fi

# ---
# Do all the actions
# ---

echo -e "
#VHOST added by script
127.0.0.1\t${PROJECTDOMAIN}" >> /etc/hosts



chmod -R 777 $PRIMARYDIR/$PROJECTNAME

echo -e "<VirtualHost *:${APACHE_PORT}>
    ServerName $PROJECTDOMAIN
    ServerAlias www.$PROJECTDOMAIN
    DocumentRoot "$PRIMARYDIR/$PROJECTNAME$PUBDIR"
    <Directory "$PRIMARYDIR/$PROJECTNAME$PUBDIR">
        AllowOverride All
        Order allow,deny
        Allow from all
    </Directory>
</VirtualHost>" > $VHOST_DIR/vhost_$PROJECTNAME.conf

chmod 777 $VHOST_DIR/vhost_$PROJECTNAME.conf

echo ===================================================
echo "Restarting apache..."
echo ===================================================
sudo /etc/init.d/apache2 restart
echo ===================================================
echo "DONE! created project '$PROJECTNAME'"
exit